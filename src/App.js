import './App.css';
import MyCalendar from './Calendar.js'

function App() {
  return (
    <div className="App">
      <MyCalendar />
    </div>
  );
}

export default App;
