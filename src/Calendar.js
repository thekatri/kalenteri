import React, { useState } from "react";
import * as dateFns from "date-fns";
import { fi } from 'date-fns/locale'
import "./Calendar.css";

const Calendar = () => {

  const [currentDate, setCurrentDate] = useState(new Date())
  const [selectedDate, setSelectedDate] = useState(new Date())

  const nextMonth = () => {
    setCurrentDate(dateFns.addMonths(currentDate, 1))
  }

  const prevMonth = () => {
    setCurrentDate(dateFns.subMonths(currentDate, 1))
  }



  const header = () => {
    const dateFormat = "MMMM yyyy"
    return (
      <div className="header row flex-middle">
        <div className="column col-start">
          <div className="icon" onClick={prevMonth}>
            chevron_left
          </div>
        </div>
        <div className="column col-center">
          <span>{dateFns.format(currentDate, dateFormat)}</span>
        </div>
        <div className="column col-end">
          <div className="icon" onClick={nextMonth}>
            chevron_right
          </div>
        </div>
      </div>
    )
  }

  const daysOfWeek = () => {
    const dateFormat = "EEEEEE";
    const days = [];
    let startDate = dateFns.startOfWeek(currentDate, { weekStartsOn: 1 });

    for (let i = 0; i < 7; i++) {
      days.push(
        <div className="column col-center" key={i}>
          {dateFns.format(dateFns.addDays(startDate, i), dateFormat, { locale: fi })}
        </div>)
      // if (i !== 0) {
      //   days.push(
      //     <div className="column col-center" key={i}>
      //       {dateFns.format(dateFns.addDays(startDate, i), dateFormat)}
      //     </div>
      //   )
      // }

      console.log(days)
    }

    return <div className="days row">{days}</div>;

  };

  const cells = () => {
    const monthStart = dateFns.startOfMonth(currentDate)
    const monthEnd = dateFns.endOfMonth(monthStart)
    const startDate = dateFns.startOfWeek(monthStart, { weekStartsOn: 1 })
    const endDate = dateFns.endOfWeek(monthEnd)

    const dateFormat = "d"
    const rows = []

    let days = []
    let day = startDate
    let formattedDate = ""

    while (day <= endDate) {
      for (let i = 0; i < 7; i++) {
        formattedDate = dateFns.format(day, dateFormat)
        const cloneDay = day
        // console.log(parse(cloneDay))
        days.push(
          <div className={`column cell ${!dateFns.isSameMonth(day, monthStart) ? "disabled" : dateFns.isSameDay(day, selectedDate)
            ? "selected" : ""}`}
            key={day}
            onClick={() => onDateClick(cloneDay)}
          >
            <span className="number">{formattedDate}</span>
            <span className="bg">{formattedDate}</span>
          </div>
        )
        day = dateFns.addDays(day, 1);
      }

      rows.push(<div className="row" key={day}>{days}</div>)
      days = []
    }
    return <div className="body">{rows}</div>

  }

  const onDateClick = day => {
    setSelectedDate(day)
    console.log(day)
  }

  return (
    <div className="calendar">
      <div>{header()}</div>
      <div>{daysOfWeek()}</div>
      <div>{cells()}</div>

    </div>
  )
}

export default Calendar